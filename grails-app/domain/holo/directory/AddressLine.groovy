package holo.directory

import holo.shared.tags.Tag
import holo.shared.refdata.RefdataValue

/**
 * Largely inspired by OASIS CIQ TC Standard xAL
  */
class AddressLine {

  String id
  Long seq
  RefdataValue type
  String value


  static hasMany = [
  ]

  static mappedBy = [
  ]

  static belongsTo = [
    owner: Address,
  ]

  static mapping = {
                 id column:'al_id', generator: 'uuid2', length:36
                seq column:'al_seq'
               type column:'al_type_rv_fk'
              value column:'al_value'
  }

  static constraints = {
          seq(nullable:false, blank:false)
         type(nullable:false, blank:false)
        value(nullable:false, blank:false)

  }
}
