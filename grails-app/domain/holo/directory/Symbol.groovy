package holo.directory

import holo.shared.tags.Tag
import holo.shared.refdata.RefdataValue

class Symbol {

  String id
  String symbol
  String priority

  static hasMany = [
  ]

  static mappedBy = [
  ]

  static belongsTo = [
    owner: DirectoryEntry,
    authority: NamingAuthority
  ]

  static mapping = {
                 id column:'sym_id', generator: 'uuid2', length:36
              owner column:'sym_owner_fk'
          authority column:'sym_authority_fk'
             symbol column:'sym_symbol'
           priority column:'sym_priority'
  }

  static constraints = {
              owner(nullable:false, blank:false)
          authority(nullable:false, blank:false)
             symbol(nullable:false, blank:false)
           priority(nullable:true, blank:false)

  }
}
