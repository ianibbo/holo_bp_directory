package holo.directory

import holo.shared.tags.Tag
import holo.shared.refdata.RefdataValue

/**
 * The relationship between a service and a directory entry
 */
class ServiceAccount {

  String id
  Service service
  DirectoryEntry accountHolder

  String accountDetails

  static graphql = true

  static mapping = {
                 id column:'sa_id', generator: 'uuid2', length:36
            service column:'sa_service'
      accountHolder column:'sa_account_holder'
     accountDetails column:'sa_account_details'
  }

  static constraints = {
           service(nullable:false, blank:false)
     accountHolder(nullable:false, blank:false)
     accountDetails(nullable:true, blank:false)
  }
}
