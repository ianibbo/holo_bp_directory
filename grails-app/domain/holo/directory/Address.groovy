package holo.directory

import holo.shared.tags.Tag
import holo.shared.refdata.RefdataValue


class Address  {

  String id
  String addressLabel

  static hasMany = [
    lines: AddressLine,
    tags:Tag
  ]

  static mappedBy = [
    lines: 'owner'
  ]

  static belongsTo = [
    owner: DirectoryEntry
  ]

  static mapping = {
                 id column:'addr_id', generator: 'uuid2', length:36
       addressLabel column:'addr_label'
  }

  static constraints = {
    addressLabel(nullable:false, blank:false)
  }
}
