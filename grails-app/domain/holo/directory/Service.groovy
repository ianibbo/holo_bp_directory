package holo.directory

import holo.shared.tags.Tag
import holo.shared.refdata.RefdataValue

/**
 * A service represents an internet callable endpont. A service can support many
 * accounts or tenants. For example, an iso 10161 service endpoint might support 
 * symbols from multiple different institutions. This class then models the service
 * itself.
 */
class Service {

  String id
  String name
  String address

  /**
   * The actual protocol in use
   */
  RefdataValue type

  /**
   * The business function served - if I want to list all services providing ILL, query this for ILL
   */
  RefdataValue businessFunction

  static hasMany = [
    tags:Tag
  ]

  static mappedBy = [
  ]

  static mapping = {
                  id column:'se_id', generator: 'uuid2', length:36
                name column:'se_name'
             address column:'se_address'
                type column:'se_type_fk'
    businessFunction column:'se_business_function_fk'
  }

  static constraints = {
                name(nullable:true, blank:false)
                type(nullable:false, blank:false)
             address(nullable:false, blank:false)
    businessFunction(nullable:true, blank:false)
  }
}
